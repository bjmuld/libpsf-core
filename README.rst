## Compiled and installable from PyPi: `pip install libpsf`
-----------------------------------
libpsf is a c++ library that reads Cadence PSF *BINARY* waveform files. There is another Python tool called "psf_utils" which reads binary PSF data.
A Python extension is available (source: https://gitlab.com/bjmuld/libpsf-python PyPI: https://pypi.org/project/libpsf/)

This project was initially begun by Henrik Johansen: https://github.com/henjo/libpsf 

Contributions welcomed.

Install
=======

Install prerequisits
--------------------

On a debian based system you can run the following to install the 
packages needed to build libpsf::

   sudo apt install autoconf automake libtool libboost-all-dev python-numpy python-setuptools 

Build and install
-----------------
To build and install the library::

   ./autogen.sh
   make
   sudo make install

To build the python extension::

   ./autogen.sh --with-python
   make
   sudo make install


Running the tests
-----------------
Install cppunit, then compile and run the tests in the test dir::

    sudo apt-get install libcppunit-dev
    cd test
    make
    ./test_psfdataset
